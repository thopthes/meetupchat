import {User} from './user';

export class ChatMessage {
  id: string;
  user: User;
  message: string;
  timestamp: number;

  // local variable only
  messageOwnership: string;

  constructor(id: string, user: User, message: string, timestamp: number, messageOwnership?: string) {
    this.id = id;
    this.user = user;
    this.message = message;
    this.timestamp = timestamp;
    this.messageOwnership = messageOwnership;
  }
}
