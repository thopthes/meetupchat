import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();

const objectKeys = Object.keys;

const animals = [
  'Albatross',
  'Alligator',
  'Anaconda',
  'Anglerfish',
  'Ant',
  'Ape',
  'Badger',
  'Barracuda',
  'Basilisk',
  'Bass',
  'Bat',
  'Bear',
  'Beaver',
  'Bee',
  'Beetle',
  'Bird',
  'Bison',
  'Boar',
  'Bobcat',
  'Butterfly',
  'Buzzard',
  'Camel',
  'Cat',
  'Centipede',
  'Chameleon',
  'Cheetah',
  'Chicken',
  'Chimpanzee',
  'Clownfish',
  'Cobra',
  'Condor',
  'Cow',
  'Coyote',
  'Crab',
  'Cricket',
  'Crocodile',
  'Crow',
  'Cuckoo',
  'Deer',
  'Dingo',
  'Dinosaur',
  'Dolphin',
  'Donkey',
  'Dove',
  'Dragonfly',
  'Dragon',
  'Duck',
  'Eagle',
  'Earthworm',
  'Elephant',
  'Falcon',
  'Ferret',
  'Firefly',
  'Flamingo',
  'Flea',
  'Fox',
  'Frog',
  'Giraffe',
  'Goat',
  'Goldfish',
  'Goose',
  'Gorilla',
  'Grasshopper',
  'Hamster',
  'Hawk',
  'Hedgehog',
  'Hornet',
  'Horse',
  'Jackal',
  'Jaguar',
  'Kangaroo',
  'Leopard',
  'Lion',
  'Lizard',
  'Monkey',
  'Moose',
  'Mosquito',
  'Mouse',
  'Orangutan',
  'Orca',
  'Owl',
  'Panda',
  'Panther',
  'Parrot',
  'Penguin',
  'Pig',
  'Pigeon',
  'Piranha',
  'Porcupine',
  'Puma',
  'Python',
  'Rabbit',
  'Raccoon',
  'Rattlesnake',
  'Raven',
  'Reindeer',
  'Scorpion',
  'Seahorse',
  'Shark',
  'Sheep',
  'Sparrow',
  'Spider',
  'Squirrel',
  'Starfish',
  'Stingray',
  'Swan',
  'Swordfish',
  'Tiger',
  'Toad',
  'Turkey',
  'Turtle',
  'Viper',
  'Vole',
  'Vulture',
  'Wasp',
  'Wolf',
  'Worm',
  'Zebra'
];

const colors = [
  '#fc5c62',
  '#fd9645',
  '#26de82',
  '#4b7bed',
  '#45aaf3',
  '#8854d1',
  '#4b6585',
  '#a55eeb',
  '#2bcbbb',
  '#a5b1c1',
  '#fd5c62',
  '#fe9645',
  '#28de82',
  '#427bed',
  '#48aaf3',
  '#8154d1',
  '#4f6585',
  '#a15eeb',
  '#22cbbb',
  '#aeb1c1'
];

function getRandomUsername() {
  return animals[Math.floor(Math.random() * animals.length)];
}

function getRandomColor() {
  return colors[Math.floor(Math.random() * colors.length)];
}

exports.userCreated = functions.auth.user().onCreate((user) => {
    const userId = user.uid;

    const updates = {};

    const randomUsername = getRandomUsername();
    const randomColor = getRandomColor();

    updates['users/' + userId + '/username'] = 'The ' + randomUsername;
    updates['users/' + userId + '/color'] = randomColor;

    return admin.database().ref()
      .update(updates
      ).then(() => {
        console.log("User: " + randomUsername + ' ' + randomColor + ' created');
        return null;
      })
      .catch((error) => {
        console.log("Error creating user: " + randomUsername + ' ' + randomColor);
        return null;
      });
});
