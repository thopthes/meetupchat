import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ChatService} from '../../shared/services/chat.service';
import {AuthService} from '../../shared/services/auth.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-message-input',
  templateUrl: './message-input.component.html',
  styleUrls: ['./message-input.component.scss']
})
export class MessageInputComponent implements OnInit, OnDestroy {

  form: FormGroup;

  private destroy$: Subject<void> = new Subject();

  constructor(private fb: FormBuilder,
              private chatService: ChatService,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      message: new FormControl({
        'value': '',
        'disabled': true
      }),
    });

    this.authService.user$
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(user => {
        if (user && user.username) {
          this.form.get('message').enable();
        } else {
          this.form.get('message').disable();
        }
      });

  }

  sendMessage() {
    if (this.form.get('message').value) {
      this.chatService.saveMessage(this.form.get('message').value);
      this.form.get('message').setValue('');
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
