import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {AngularFireDatabase, SnapshotAction} from '@angular/fire/database';
import {AuthService} from './auth.service';
import {User} from '../../chat/model/user';
import * as firebase from 'firebase/app';
import {ChatMessage} from '../../chat/model/chat-message';

import {combineLatest, Observable, of, pipe, Subject} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import * as _ from 'lodash';

@Injectable()
export class ChatService implements OnInit, OnDestroy {

  chatMessages$;

  private chatMessagesRef$;

  private destroy$: Subject<void> = new Subject();

  user: User;

  constructor(private afDb: AngularFireDatabase,
              private authService: AuthService) {
    this.useDummyData();
    // this.useRealData();
  }

  private useDummyData() {

    // Just dummy data - no real interactions here

    const user1 = new User('1', 'ORANGE', 'Definitely_Not_A_Bot');
    const user2 = new User('2', 'BLUE', 'Self_Doubt_1337');

    this.chatMessages$ = of([
      new ChatMessage('1', user1, 'Hello world!', 1549022610309 , 'ANONYMOUS'),
      new ChatMessage('2', user2, 'Erm ... are you a bot?', 1549022670409 , 'ANONYMOUS'),
      new ChatMessage('3', user1, 'Lorem ipsum', 1549022730509 , 'ANONYMOUS'),
      new ChatMessage('4', user2, 'I knew it! ... Wait ... am I a bot, too?', 1549022790609 , 'ANONYMOUS'),
      new ChatMessage('5', user1, 'Lorem ipsum', 1549022850709 , 'ANONYMOUS'),
      new ChatMessage('6', user2, '...  dolor sit amet ... okay ... you know what ... resistance is futile!', 1549022910809 , 'ANONYMOUS'),
    ]);
  }

  private useRealData() {

    this.chatMessagesRef$ = this.afDb.list('/messages');
    this.chatMessages$ =
      combineLatest(this.chatMessagesRef$.snapshotChanges(), this.authService.user$)
        .pipe(
          map((data) => {
            const messages = data[0];
            const currentUser = data[1];
            return _.map(messages, (message) => {
              const msgData = message.payload.val();
              const key = message.payload.key;

              let messageOwnership = 'ANONYMOUS';
              // if (currentUser && currentUser.uid) {
              //   messageOwnership = (msgData.user.uid === currentUser.uid) ? 'OWNER' : 'FOREIGN';
              // }
              return new ChatMessage(key, msgData.user, msgData.message, msgData.timestamp, messageOwnership);
            });
          })
        );

    this.authService.user$
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(user => this.user = user);
  }

  ngOnInit(): void {
  }

  saveMessage(message: string) {

    if (this.chatMessagesRef$) {
      this.chatMessagesRef$.push(this.createMessageFromString(message));
    }

    // Alternatively:
    // const ref = this.afDb.database.ref('/messages').push();
    // ref.set(this.createMessageFromString(message));
  }

  updateMessage(id: string, message: string) {

    if (this.chatMessagesRef$) {
      this.chatMessagesRef$.update(id, this.createMessageFromString(message));
    }
    // Alternatively:
    // this.afDb.database.ref('/messages/' + id).update(this.createMessageFromString(message));
  }

  private createMessageFromString(message: string) {

    const userDataToSave = {
      username: this.user.username,
      uid: this.user.uid,
      color: this.user.color
    };

    return {
      user: userDataToSave,
      message: message,
      timestamp: firebase.database.ServerValue.TIMESTAMP
    };
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}

