export class User {
  uid: string;
  color: string;
  username: string;
  name: string;
  photoUrl: string;

  constructor(uid: string, color?: string, username?: string, name?: string, photoUrl?: string) {
    this.uid = uid;
    this.color = color;
    this.username = username;
    this.name = name;
    this.photoUrl = photoUrl;
  }
}

