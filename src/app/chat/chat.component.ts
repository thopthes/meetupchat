import {Component, OnInit} from '@angular/core';
import {ChatService} from '../shared/services/chat.service';
import {AngularFireList} from '@angular/fire/database';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  chatMessages$: AngularFireList<{}>;

  constructor(private chatService: ChatService) {
  }

  ngOnInit() {
    this.chatMessages$ = this.chatService.chatMessages$;
  }

}
