import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {Observable, Subject} from 'rxjs';
import {User} from '../chat/model/user';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  user: User;
  private destroy$: Subject<void> = new Subject();

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.authService.user$
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(user => this.user = user);
  }

  signInViaGoogle() {
    this.authService.googleLogin();
  }

  signOut() {
    this.authService.signOut();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
