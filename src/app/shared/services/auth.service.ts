import {Injectable} from '@angular/core';
import {EMPTY, Observable, of} from 'rxjs';
import * as firebase from 'firebase/app';
import {catchError, map, switchMap} from 'rxjs/operators';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireAuth} from '@angular/fire/auth';
import {User} from '../../chat/model/user';

@Injectable()
export class AuthService {

  user$: Observable<User>;

  constructor(private afAuth: AngularFireAuth,
              private adb: AngularFireDatabase) {
    this.user$ = this.afAuth.authState
      .pipe(
        switchMap(user => {
          if (user) {
            return this.adb.object(`users/${user.uid}`)
              .valueChanges()
              .pipe(
                map(userData => {
                  return this.combineUserData(user, userData);
                }), catchError(error => {
                  return of(null);
                })
              );
          } else {
            return of(null);
          }
        })
      );
  }

  private combineUserData(user: firebase.User, userData: any) {
    return {
      ...userData,
      photoUrl: user.photoURL,
      name: user.displayName
    };
  }

  // Login/Signup
  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();

    this.afAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        this.setUserData(credential.user);
      }).catch(error => {
      console.log('AUTH ERROR', error);
    });
  }

  // Set user data
  private setUserData(user) {
    this.adb
      .object(`users/${user.uid}`)
      .update({
      uid: user.uid
    });
  }

  // Logout/Signout
  signOut() {
    this.afAuth.auth.signOut();
  }

}
