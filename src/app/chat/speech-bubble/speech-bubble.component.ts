import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ChatService} from '../../shared/services/chat.service';
import {ChatMessage} from '../model/chat-message';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-speech-bubble',
  templateUrl: './speech-bubble.component.html',
  styleUrls: ['./speech-bubble.component.scss']
})
export class SpeechBubbleComponent implements OnInit {

  editing = false;

  messageOwnership: string;

  form: FormGroup;

  bubbleStyle: string;

  @Input() chatMessage: ChatMessage;

  constructor(private fb: FormBuilder,
              private chatService: ChatService) {
  }

  ngOnInit() {
    this.messageOwnership = this.chatMessage.messageOwnership;
    this.form = this.fb.group({
      message: new FormControl(this.chatMessage.message)
    });

    this.initStyles();
  }

  startEditMode() {
    if (this.messageOwnership === 'OWNER') {
      this.editing = true;
    }
  }

  stopEditMode() {
    this.editing = false;
  }

  updateMessage() {
    this.stopEditMode();
    this.chatService.updateMessage(this.chatMessage.id, this.form.get('message').value);
  }

  private initStyles() {
    switch (this.messageOwnership) {
      case 'OWNER':
        this.bubbleStyle = 'speech-bubble-right hoverable';
        break;
      case 'FOREIGN':
        this.bubbleStyle = 'speech-bubble-left';
        break;
      case 'ANONYMOUS':
      default:
        this.bubbleStyle = 'speech-bubble-center';
        break;
    }
  }
}
