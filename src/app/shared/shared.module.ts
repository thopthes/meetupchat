import {ModuleWithProviders, NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {ChatService} from './services/chat.service';
import {AuthService} from './services/auth.service';
import {AutofocusDirective} from './directives/auto-focus.directive';


@NgModule({
  declarations: [
    AutofocusDirective
  ],
  imports: [
    CommonModule,
    TranslateModule
  ],
  exports: [
    CommonModule,
    TranslateModule,
    AutofocusDirective
  ]
})

export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        ChatService,
        AuthService
      ]
    };
  }
}
